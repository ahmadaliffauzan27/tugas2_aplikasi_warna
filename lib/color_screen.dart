import 'package:flutter/material.dart';

class ColorScreen extends StatefulWidget {
  const ColorScreen({super.key});

  @override
  State<ColorScreen> createState() => _ColorScreenState();
}

class _ColorScreenState extends State<ColorScreen> {
  int _currentColorIndex =
      0; // Inisialisasi index defaultnya adalah 0, 0 disini berarti adalah warna merah

  // membuat list untuk warna
  final List<Color> _box = [
    Colors.red, // index 0
    Colors.yellow, // index 1
    Colors.green, // index 2
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 100),
                child: Container(
                  width: 150,
                  height: 150,
                  color: _box[
                      _currentColorIndex], // warna kotak akan diisi berdasarkan index yang ditentukan, nilai defaultnya adalah 0 atau warna merah
                ),
              ),
              const SizedBox(
                height: 100,
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.amber)),
                  onPressed: () {
                    setState(() {
                      _currentColorIndex = (_currentColorIndex + 1) %
                          _box.length; // menambahkan _currentColorIndex dengan nilai 1 sehingga index akan berubah jika tombol diklik
                    });
                  },
                  child: const Center(
                    child: Text(
                      'Ubah Warna',
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
